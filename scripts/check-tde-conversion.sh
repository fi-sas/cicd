#!/bin/sh

CONFIG_NAME="sasocial_postgres_tde_converted"

if ! docker config ls --format '{{.Name}}' | grep -q "^${CONFIG_NAME}$"; then
  echo "Error: It appears that this server has not yet been converted to Postgres TDE."
  exit 1
fi